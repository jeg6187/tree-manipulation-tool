/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';

const StyledButton = styled(animated.button)`
  font-size: 1em;
  outline-width: 0;
  width: ${props => props.width};
  height: ${props => props.height};
  border: 2px solid #e6e6e6;
  background-color: #e6e6e6;
  border-radius: 50px;
  &&:hover {
    cursor: pointer;
  }
`;

function Button({
  name,
  type,
  handleClick,
  style,
  width,
  height,
  shadow,
  value,
  children
}) {
  const [flag, setFlag] = useState({
    mouseActive: false,
    mouseHover: false
  });

  const props = useSpring({
    borderColor: flag.mouseHover ? '#63a70a' : '#e6e6e6',
    transform: `scale(${flag.mouseActive ? 0.9 : 1})`,
    backgroundColor: flag.mouseActive ? '#63a70a' : '#e6e6e6',
    boxShadow:
      shadow && !flag.mouseActive
        ? '0px 0px 7px #595959'
        : '0px 0px 0px #595959',
    from: {
      borderColor: '#e6e6e6',
      transform: `scale(1)`,
      backgroundColor: '#e6e6e6',
      boxShadow: shadow ? '0px 0px 7px #595959' : '0px 0px 0px #595959'
    }
  });

  const handleMouseDown = e => setFlag({ ...flag, mouseActive: true });
  const handleMouseUp = e =>
    setTimeout(() => setFlag({ ...flag, mouseActive: false }), 100);
  const handleMouseOver = e =>
    setFlag({ ...flag, mouseHover: true, mouseActive: false });
  const handleMouseOut = e =>
    setFlag({ ...flag, mouseHover: false, mouseActive: false });

  return (
    <StyledButton
      style={{ ...props, ...style }}
      name={name}
      type={type}
      value={value}
      onMouseDown={handleMouseDown}
      onMouseUp={handleMouseUp}
      onMouseOver={handleMouseOver}
      onMouseOut={handleMouseOut}
      onClick={handleClick}
      width={width}
      height={height}
    >
      {children}
    </StyledButton>
  );
}

Button.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  handleClick: PropTypes.func,
  style: PropTypes.objectOf(PropTypes.any),
  width: PropTypes.string,
  height: PropTypes.string,
  shadow: PropTypes.bool,
  // eslint-disable-next-line react/forbid-prop-types
  value: PropTypes.any,
  // eslint-disable-next-line react/forbid-prop-types
  children: PropTypes.any
};

Button.defaultProps = {
  type: 'button',
  handleClick: undefined,
  style: undefined,
  width: undefined,
  height: undefined,
  shadow: true,
  value: undefined,
  children: undefined
};

export default Button;
