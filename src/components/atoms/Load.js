import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';
import { useSpring, animated } from 'react-spring';

import logo from '../../assets/logo.svg';

const LoadContainer = styled(animated.div)`
  display: ${props => (props.loaded ? 'none' : 'flex')};
  align-items: center;
  justify-content: center;
  position: ${props => props.position};
  width: 100%;
  height: 100%;
  z-index: 1000;
  top: 0px;
  bottom: 0px;
  left: 0px;
  right: 0px;
  background-color: ${props => props.color};
  margin: ${props => props.margin};
`;

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const Img = styled.img`
  width: 10em;
  height: 10em;
  animation: ${rotate} 2s linear infinite;
`;

const Load = ({ loaded, margin, color, position }) => {
  const loadContainerRef = React.createRef();
  const loadContainerProps = useSpring({
    opacity: loaded ? 0 : 1,
    from: { opacity: 1 }
  });

  useEffect(() => {
    if (loaded) {
      const curLoadContainer = loadContainerRef.current;
      setTimeout(() => {
        curLoadContainer.style.display = 'none';
      }, 500);
    }
  }, [loaded]);

  return (
    <LoadContainer
      style={loadContainerProps}
      margin={margin}
      color={color}
      ref={loadContainerRef}
      position={position}
    >
      <Img alt="loadSVG" src={logo} />
    </LoadContainer>
  );
};

Load.propTypes = {
  loaded: PropTypes.bool.isRequired,
  margin: PropTypes.string,
  color: PropTypes.string,
  position: PropTypes.string
};

Load.defaultProps = {
  margin: undefined,
  color: '#f2f2f2',
  position: 'relative'
};

export default Load;
