/* eslint-disable no-unused-vars */
import { groupBy, sortBy, cloneDeep } from 'lodash';
import { get } from './restRequests';

// gets treeView Data from the database based on the batchID
export const getTreeView = batchId =>
  get(
    `https://downline-aks-test.nspnet.net/api/v1/downlineanalysis/viewtree/${batchId}`
  );

// gets the batchID for the specified accountNumber
export const getAnalysisBatchId = accountNumber =>
  get(
    `https://downline-aks-test.nspnet.net/api/v1/downlineanalysis/batchid?accountNumber=${accountNumber}`
  );

// gets the details of a certain batch from  batchID
export const getAccountDetails = batchId =>
  get(
    `https://downline-aks-test.nspnet.net/api/v1/downlineanalysis/accountdetails/${batchId}`
  );

// combines two arrays of objects based on whatever params you set in valCheck function
export const combineArrayObj = (arrayA, arrayB, valChecker = undefined) => {
  return cloneDeep(arrayA).map(itemA => {
    cloneDeep(arrayB).forEach(itemB => {
      if (valChecker(itemA, itemB)) {
        Object.assign(itemA, { ...itemB });
      }
    });
    return itemA;
  });
};

// gets the window dimensions of the browser
export const getWindowDimensions = () => ({
  width: window.innerWidth,
  height: window.innerHeight
});

// this function determines the order of which nodes are organized in the tree
export const organizeData = fData => {
  const result = [];
  // todo: replace dataSource:fData with dataSource:cloneDeep(fData)
  // ! requires major reconstruction of the treeComp for the time being
  // * maybe some sort of global state to indicate what it is?
  const dataSource = fData;
  const groupByLevel = groupBy(dataSource, o => o.depthLevel);
  const groupBySponsor = groupBy(dataSource, o => o.sponsorAccountNumber);
  Object.keys(groupByLevel).forEach(levelKey => {
    const levelResult = [];
    const curSelLevel = groupBy(
      groupByLevel[levelKey],
      o => o.sponsorAccountNumber
    );
    Object.keys(curSelLevel).forEach(sponsorKey => {
      const curSelSponsor = curSelLevel[sponsorKey];
      curSelSponsor.forEach(node => {
        const children = groupBySponsor[node.accountNumber] || null;
        Object.assign(node, {
          children,
          canvasPosition: {
            current: { x: 0, y: 0 },
            previous: { x: 0, y: 0 }
          },
          linePoints: {
            current: [0, 0, 0, 0],
            previous: [0, 0, 0, 0]
          }
        });
      });
      levelResult.push(sortBy(curSelSponsor, n => n.accountNumber));
    });
    result.push(levelResult);
  });
  return result;
};

// if you need to flatten organizedData, this is your function
export const flattenOrganizedData = oData => {
  let result = [];
  oData.forEach(level => {
    level.forEach(sponsor => {
      result = result.concat(sponsor);
    });
  });
  return result;
};

// gets the children of a desired element
export const getChildrenElements = (accountNumber, fData) => {
  const result = fData.filter(n => n.sponsorAccountNumber === accountNumber)
    .children;
  return result.length > 0 ? result : null;
};

// takes any account number and returns the respective account
export const getAccount = (accountNumber, fData) => {
  const result = fData.filter(n => n.accountNumber === accountNumber);
  return result.length > 0 ? result[0] : null;
};

// if you need to push formatedData to the database, use this
export const stripFormatedData = fData =>
  cloneDeep(fData).map(node => ({
    analysisBatchId: node.analysisBatchId,
    accountNumber: node.accountNumber,
    sponsorAccountNumber: node.sponsorAccountNumber,
    position: node.position,
    depthLevel: node.depthLevel,
    countryCode: node.countryCode
  }));

// if you need to push organizedData to the database, use this
export const stripOrganizedData = oData =>
  stripFormatedData(flattenOrganizedData(oData));
