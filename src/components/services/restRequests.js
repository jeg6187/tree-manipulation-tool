import Axios from 'axios';
import { Observable } from 'rxjs';

// basically just observables for the three main requests

export const get = (url, params = null) =>
  Observable.create(observer => {
    Axios.get(url, params).then(promiseObject => {
      // eslint-disable-next-line no-unused-expressions
      promiseObject.status === 200
        ? observer.next(promiseObject.data)
        : observer.error(promiseObject.statusText);
      observer.complete();
    });
  });

export const post = (url, params = null) =>
  Observable.create(observer => {
    Axios.post(url, params).then(promiseObject => {
      // eslint-disable-next-line no-unused-expressions
      promiseObject.status === 200
        ? observer.next(promiseObject.data)
        : observer.error(promiseObject.statusText);
      observer.complete();
    });
  });

export const put = (url, params = null) =>
  Observable.create(observer => {
    Axios.put(url, params).then(promiseObject => {
      // eslint-disable-next-line no-unused-expressions
      promiseObject.status === 200
        ? observer.next(promiseObject.data)
        : observer.error(promiseObject.statusText);
      observer.complete();
    });
  });
